import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Users\\Hp\\Downloads\\DemoAppV2 (1).apk', true)

Mobile.tap(findTestObject('Object Repository/Register/Button - Login Here'), 0)

Mobile.tap(findTestObject('Object Repository/Register/TextView - Register, now'), 0)

Mobile.setText(findTestObject('Object Repository/Register/SetText - Nama'), 'Robbi', 0)

Mobile.tap(findTestObject('Object Repository/Register/Button Logo Calender'), 0)

Mobile.tap(findTestObject('Object Repository/Register/Button - OKE Calender'), 0)

Mobile.setText(findTestObject('Object Repository/Register/Set Text Email'), 'robbifirli12@gmail.com', 0)

Mobile.setText(findTestObject('Object Repository/Register/Set Text Whatsapp'), '082173181423', 0)

Mobile.setEncryptedText(findTestObject('Object Repository/Register/Set Text Password'), 'tzH6RvlfSTg=', 0)

Mobile.setEncryptedText(findTestObject('Object Repository/Register/Set Text Confirmation Password'), '5v8hUXfQu8ylf0sBGmc/Og==', 
    0)

Mobile.tap(findTestObject('Object Repository/Register/CheckBox terms and conditions'), 0)

Mobile.verifyElementVisible(findTestObject('Register/TextView - Password atleast must contain alphanumeric (a-Z,0-9) with minim 8 characters'), 
    0)

Mobile.closeApplication()

