<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>Layout Pilih Akun</name>
   <tag></tag>
   <elementGuidId>00000000-0000-0000-0000-000000000000</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>android.widget.LinearLayout</value>
      <webElementGuid>c4af6508-5795-4427-aa5a-b4109b2118ac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>index</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>6baf2d11-bc40-4b39-a39b-9f2b82993e92</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>resource-id</name>
      <type>Main</type>
      <value>com.google.android.gms:id/account_picker_container</value>
      <webElementGuid>d1d8b033-b64f-407c-a5d3-d3a0fd6a2b3d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>package</name>
      <type>Main</type>
      <value>com.google.android.gms</value>
      <webElementGuid>592f769f-c3ec-4696-97b1-d4a74f7fa589</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>checkable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>ac95ec22-0890-42ce-8775-9f21e7e2b203</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>checked</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>93a551a8-591a-4e88-96c3-bb94f7db6177</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>clickable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>02076898-1b07-41b8-b79c-7bc67d5eff49</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>enabled</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>a15fcfd4-eafb-4a12-92f6-fe59aa018b49</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>focusable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>7050d63f-9a0a-4ee5-85b9-3d11eee66f98</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>focused</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>d5aed50c-eb39-4d4b-9812-b836fcd694fa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>scrollable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>929a643f-80a3-4d72-8b8a-5dd5cd81e7e2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>long-clickable</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>6642167f-31af-49de-891a-603c71a7fd82</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>password</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>29b98a9c-cac8-4192-b29e-52dd9f40ced2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>selected</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>a1c015bd-c9e6-414d-8a10-4b59f5d3faed</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>x</name>
      <type>Main</type>
      <value>72</value>
      <webElementGuid>b0a54aaa-34c3-4e8f-b1ef-199dff277d3c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>y</name>
      <type>Main</type>
      <value>580</value>
      <webElementGuid>5b528fc7-caa6-4b66-b147-25df1f4eecf5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>936</value>
      <webElementGuid>e771ab0b-c367-4995-b45f-d47034431577</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>1147</value>
      <webElementGuid>dfe49736-8d0e-4375-b93f-f440f8cd46b3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>bounds</name>
      <type>Main</type>
      <value>[72,580][1008,1727]</value>
      <webElementGuid>3c2e4df0-e394-4740-a0f2-f5c9a8b0c6d8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>displayed</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>6511ce83-92aa-4a18-980f-a70586415282</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value></value>
      <webElementGuid>99bbfb8f-60b2-49e2-a498-dfe023d26705</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//hierarchy/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]</value>
      <webElementGuid>bf635c57-5e52-4601-b953-e6ad379d1094</webElementGuid>
   </webElementProperties>
   <locator>//*[@class = 'android.widget.LinearLayout' and @resource-id = 'com.google.android.gms:id/account_picker_container' and (@text = '' or . = '')]</locator>
   <locatorStrategy>ATTRIBUTES</locatorStrategy>
</MobileElementEntity>
